# QR Code Generator

### What is an qrcode
A QR code is a type of matrix barcode invented in 1994 by the Japanese automotive company Denso Wave. A barcode is a machine-readable optical label that contains information about the item to which it is attached. In practice, QR codes often contain data for a locator, identifier, or tracker that points to a website or application.

### Requirements 
    > 1. Python 3.5 or higher (https://www.python.org/) 
    > 2. PyQRCode (https://pypi.org/project/PyQRCode/)
    > 3. Pillow (https://pypi.org/project/Pillow/)
    > 4.PyPng (https://pypi.org/project/pypng/)

![alt text](https://xp.io/storage/1AGCParX.png)
### How does it work?
    
_It's a GUI in which you put the link to the webpage you want to convert into a QR code and then it would pop up on the screen the QR code so you can scan it and use it._
    

![alt text](https://xp.io/storage/1AGvnRpF.png)

### Final thoughts 

Thanks for reading. I hope this tutorial was helpful,  If you like this post, probably you might like my next ones, 





